function [FR,FT,FA,Fr_phase]=Fresnel(lambda,thetai,h,n,pol)

% lambda = 200;
% n = 1 +1i;
% thetai = 0;
% h = 10;
% pol = 1;

%Snell's law:
theta(1)=thetai*pi/180;
for a=1:length(n)-1
    theta(a+1)=real(asin(n(a)/n(a+1)*sin(theta(a))))-1i*abs(imag(asin(n(a)/n(a+1)*sin(theta(a)))));
end

%Fresnel coefficients:
if pol==0 %formulas for s polarization
    for a=1:length(n)-1
        Fr(a)=(n(a)*cos(theta(a))-n(a+1)*cos(theta(a+1)))/(n(a)*cos(theta(a))+n(a+1)*cos(theta(a+1)));
        Ft(a)=2*n(a)*cos(theta(a))/(n(a)*cos(theta(a))+n(a+1)*cos(theta(a+1)));
    end
elseif pol==1 %formulas for p polarization
    for a=1:length(n)-1
        Fr(a)=(n(a)*cos(theta(a+1))-n(a+1)*cos(theta(a)))/(n(a)*cos(theta(a+1))+n(a+1)*cos(theta(a)));
        Ft(a)=2*n(a)*cos(theta(a))/(n(a)*cos(theta(a+1))+n(a+1)*cos(theta(a)));
    end
end

%phase shift factors:
for a=1:length(n)-2
    delta(a)=2*pi*h(a+1)/lambda*n(a+1)*cos(theta(a+1));
end

%build up transfer matrix:
M=[1,0;0,1]; %start with unity matrix
for a=1:length(n)-2
    M=M*1/Ft(a)*[1,Fr(a);Fr(a),1]*[exp(-1i*delta(a)),0;0,exp(1i*delta(a))];
end
M=M*1/Ft(length(n)-1)*[1,Fr(length(n)-1);Fr(length(n)-1),1];

%total Fresnel coefficients:
Frtot=M(2,1)/M(1,1);
Fttot=1/M(1,1);

%special case of single interface:
if length(n)==2
    Frtot=Fr(1);
    Fttot=Ft(1);
end

%total Fresnel coefficients in intensity:
FR=(abs(Frtot))^2;
FT=(abs(Fttot))^2*real(n(length(n))*cos(theta(length(n))))/real(n(1)*cos(theta(1)));
FA=1-FR-FT;
Fr_phase = Frtot;

end