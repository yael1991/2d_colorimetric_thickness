function epsilon=Au(lambda)

%analytical formula for gold based on wavelength in nm, fits J&C data:
epsiloninf=1.54;
lambdap=143;
gammap=14500;
A1=1.27;
lambda1=470;
phi1=-pi/4;
gamma1=1900;
A2=1.1;
lambda2=325;
phi2=-pi/4;
gamma2=1060;

%other parameters, worse fit to J&C but seems more accurate often:
%epsiloninf=1.53;
%lambdap=155;
%gammap=17000;
%A1=0.94;
%lambda1=468;
%phi1=-pi/4;
%gamma1=2300;
%A2=1.36;
%lambda2=331;
%phi2=-pi/4;
%gamma2=940;

for a=1:length(lambda)
    epsilon(a)=epsiloninf-1/(lambdap^2*(1/lambda(a)^2+1i/(gammap*lambda(a))))...
        +A1/lambda1*(exp(phi1*1i)/(1/lambda1-1/lambda(a)-1i/gamma1)+ ...
        exp(-phi1*1i)/(1/lambda1+1/lambda(a)+1i/gamma1))...
        +A2/lambda2*(exp(phi2*1i)/(1/lambda2-1/lambda(a)-1i/gamma2)+ ...
        exp(-phi2*1i)/(1/lambda2+1/lambda(a)+1i/gamma2));
end

end