clear;
addpath('./color_source/');
addpath('./color_toolbox/');
addpath('./Transfer-Matrix-Method/');

Lab_color_look = rgb2lab(color_look, 'ColorSpace','adobe-rgb-1998');

lambda_interpolated = [400 : 1 : 800];

data = load(['file_refractive_index_2D_material.txt']); % lambda n k
lambda_2D_material = data(:,1);
n_2D_material =  data(:,2);
k_2D_material =  data(:,3);
n_2D_material = interp1(lambda_2D_material, n_2D_material, lambda_interpolated);
k_2D_material = interp1(lambda_2D_material, k_2D_material, lambda_interpolated);
m_2D_material = n_2D_material + 1i*k_2D_material;

data_Si = load('file_refractive_index_Si.txt'); % lambda n k
lambda = data_Si(:,1);
n_Si =  data_Si(:,2);
k_Si =  data_Si(:,3);
n_Si = interp1(lambda, n_Si, lambda_interpolated);
k_Si = interp1(lambda, k_Si, lambda_interpolated);
m_Si = n_Si + 1i*k_Si;

data_SiO2 = load('file_refractive_index_SiO2.txt'); % lambda n k
lambda = data_SiO2(:,1);
n_SiO2 =  data_SiO2(:,2);
k_SiO2 =  data_SiO2(:,3);
n_SiO2 = interp1(lambda, n_SiO2, lambda_interpolated);
k_SiO2 = interp1(lambda, k_SiO2, lambda_interpolated);
m_SiO2 = n_SiO2 + 1i*k_SiO2;

h_2D_material =  [0:300]; % thickness of the 2D material
h_SiO2 = 300; % thickness of the SiO2
NA = 0.25; % numerical aperture of the objective
angle_NA = round(asin(0.25)*180/pi);
angle = (-angle_NA : 1 : angle_NA);
theta = 0; pol = 1;
figure('Position', [0 0 2000 100]); hold on;
delta_E = 100000000;
thick_final = 0;
FR_s_total = [];
FR_p_total = [];
for i = 1 : length(h_2D_material)
    
    for j = 1 : length(angle)
        for l = 1 : length(lambda_interpolated)
            H = [NaN, h_2D_material(i), h_SiO2, NaN]; % thicknesses of the multilayers
            n = [1, m_2D_material(l), m_SiO2(l), m_Si(l)]; % refractive indices of the multilayers
            [FR_p(l),FT_p(l),~ ] = Fresnel(lambda_interpolated(l), angle(j), H, n, 0); % R and T for s-polarization
            [FR_s(l),FT_s(l),~ ] = Fresnel(lambda_interpolated(l), angle(j), H, n, 1); % R and T for p-polarization
        end
        FR_s_total(j,:) = FR_p*normpdf(angle(j),0,angle_NA);
        FR_p_total(j,:) = FR_s*normpdf(angle(j),0,angle_NA);
    end
    
    % NA averaging
    FR_s_total_int = trapz(angle',FR_s_total',2)./trapz(angle',normpdf(angle,0,angle_NA),2);
    FR_p_total_int = trapz(angle',FR_p_total',2)./trapz(angle',normpdf(angle,0,angle_NA),2);
    
    FR = (FR_s_total_int + FR_p_total_int)/2; % Unpolarized light
    
    % Calculating color coordinates for 'D65' illuminant and '10' degrees CIE 1964
    % this parameters can be changed. So far A illumint and '2' degrees
    % CIE1964 are implemented. Others can be easility included in the
    % color_source toolbox
    [~, ~, ~, ~, ~, ~, L, a, b, C, h] = coordenadas_color('D65', ...
        [lambda_interpolated' FR], '10');
    color_dot_Lab = [L, a, b]; % color coordinate in Lab
    color_dot = lab2rgb([L, a, b],'ColorSpace','adobe-rgb-1998'); % color coordinate in RGB
    
    % We create a matrix that stores for each thickness the RGB color coordinate
    color_matrix(i,:) = [h_2D_material(i) color_dot];
    
    % Ploting color vs thickness
    if (color_dot(1)>0 && color_dot(2)>0 && color_dot(3)>0 && ...
            color_dot(1)<1 && color_dot(2)<1 && color_dot(3)<1);
        scatter(h_2D_material(i), 1, 1000, color_dot, 's', 'markerfacecolor', color_dot);
    end
  i  
end
xlim([-10 310]);
set(gca, 'fontsize', 24, 'yticklabel', {}, 'linewidth', 2); box on;
set(gca, 'Xtick', [0:10:300], 'XtickLabel', {'0', '','','','', '50', ...
    '','','','', '100','','','','', '150','','','','', '200', ...
    '','','','', '250','','','','', '300'})
set(gca, 'fontsize', 24, 'yticklabel', {})
print('-djpeg', '-r600', ['your_color_bar.jpg']);
dlmwrite('thickness_colorcoordinates_matrix.txt', color_matrix, '\t');