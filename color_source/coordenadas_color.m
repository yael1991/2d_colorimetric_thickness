function [X, Y, Z, ref_X, ref_Y, ref_Z, L, a, b, C, h] = coordenadas_color(iluminante,espectro_R,observador)

%% Illuminant
data_iluminante = load(['Iluminante_' iluminante '.txt']);

%% Observer
switch observador
    case '2'
        espectro_observador = load(['Observador_2_grados_CIE_1931.txt']);
    case '10'
        espectro_observador = load(['Observador_10_grados_CIE_1964.txt']);
end

%% Interpolation
espectro_iluminante = interp1(data_iluminante(:,1),data_iluminante(:,2),espectro_observador(:,1),[],'extrap');
espectro_R_interp = interp1(espectro_R(:,1),espectro_R(:,2),espectro_observador(:,1),[],'extrap');

%% XYZ coordinates
K = 100/sum(espectro_iluminante.*espectro_observador(:,3));
X = K*sum(espectro_iluminante.*espectro_R_interp.*espectro_observador(:,2));
Y = K*sum(espectro_iluminante.*espectro_R_interp.*espectro_observador(:,3));
Z = K*sum(espectro_iluminante.*espectro_R_interp.*espectro_observador(:,4));

%% Lab coordinates
ref_X = K*sum(espectro_iluminante.*ones(size(espectro_iluminante)).*espectro_observador(:,2));
ref_Y = K*sum(espectro_iluminante.*ones(size(espectro_iluminante)).*espectro_observador(:,3));
ref_Z = K*sum(espectro_iluminante.*ones(size(espectro_iluminante)).*espectro_observador(:,4));
var_X = X/ref_X;
var_Y = Y/ref_Y;
var_Z = Z/ref_Z;

if var_X > 216/24389
    var_X = var_X.^(1/3);
else
    var_X = (841/108*var_X) + (16/116);
end
if var_Y > 216/24389
    var_Y = var_Y.^(1/3);
else
    var_Y = (841/108*var_Y) + (16/116);
end
if var_Z > 216/24389
    var_Z = var_Z.^(1/3);
else
    var_Z = (841/108*var_Z) + (16/116);
end

L = (116*var_Y) - 16;
a = 500*(var_X - var_Y);
b = 200*(var_Y - var_Z);
C = sqrt(a.^2+b.^2);
h = atan2d(b,a);