clear;
addpath('./color_source/');
addpath('./color_toolbox/');
addpath('./Transfer-Matrix-Method/');

Lab_color_look = rgb2lab(color_look, 'ColorSpace','adobe-rgb-1998');

% First you generate the color bar. During this process a txt file is
% generated. This is the file that should be read here.
data = load('thickness_colorcoordinates_matrix.txt');

% RGB color coordinate stracted from the micrograph to calculated the
% thickness of the 2D material at this point.
color_look = [142 179 163]/255; 


delta_E = 100000000;
thick_final = 0;
delta = []
for i = 1 : length(data)
    color = data(i,[2:4]); 
    color_dot_Lab = rgb2lab(color, 'ColorSpace','adobe-rgb-1998');
    delta_E_now = sqrt((color_dot_Lab(1)-Lab_color_look(1))^2 + ...
        (color_dot_Lab(2)-Lab_color_look(2))^2 + ...
        (color_dot_Lab(3)-Lab_color_look(3))^2);
    if delta_E_now < delta_E
        delta_E = delta_E_now;
        thick_final = data(i,1);
    end
    delta(i) = delta_E_now;
end


extracted_thickness = thick_final; % determined thickness
error = delta_E; % corresponding minimum color difference