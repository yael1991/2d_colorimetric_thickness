# 2D_colorimetric_thickness

We provide a colorimetric method based on optical imaging for the quantitavie determination of the thickness of exfoliated 2D materials on arbitrary substrates.

## Requirements

- MATLAB

## Code

- **generate_color_bar.m** calculates the aparent color in reflection of a 2D material on a arbitrary substrate as a function of its thickness. _Inputs_ include thicknesses and refractive indices of the multilayer system, illuminant (light sources of the microscope), observer, numerical aperture of the objective. _Generates_ your_color_bar.jpg (plot of the apparent color as a function of the thickness of the 2D material) and thickness_colorcoordinates_matrix.txt (file containing a matrix with the RGB coordinates for each thickness of the 2D material).
- **calculate_thickness.m** _Inputs_ include RGB color coordinates extracted from the micrograph image and for which the thickness of the 2D materials needs to be extracted and thickness_colorcoordinates_matrix.txt. _Generates_ the estimated thickness.
